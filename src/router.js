const router = require('express').Router()

/*
 * Controllers
 * */
const index = require('./controllers/indexController')
const book = require('./controllers/bookController')


/*
 * Router
 * */
router.get('/', index.root)
router.get('/books', book.index)
router.post('/books', book.create)
router.put('/books/:id', book.update)

router.use(index['404'])
router.use(index['500'])

module.exports = router
