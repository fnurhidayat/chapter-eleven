const express = require('express')
const logger = require('morgan')
const app = express()
const dotenv = require('dotenv')
dotenv.config()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

const router = require('./router') 
app.use(router)

module.exports = app
