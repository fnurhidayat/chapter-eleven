const mock = require('../utils/request')
const bookController = require('../../src/controllers/bookController')
const { Book } = require('../../src/models')
const book = require('../fixtures/book')
const database = require('../supports/database.js')

describe('bookController', () => {
  database.clean(Book)

  describe("update", () => {
    it('invoke res.json and res.status with 200', async done => {
      const sample = await Book.create(book.build())
      const newProps = book.build()
      const req = mock.request({
        body: newProps,
        params: { id: sample.id }
      })
      const res = mock.response()
      await bookController.update(req, res)
      
      expect(res.json).toBeCalledWith({
        status: true,
        message: 'Book updated!',
        data: expect.objectContaining({
          id: sample.id,
          title: newProps.title,
          synopsis: newProps.synopsis,
          price: newProps.price
        })
      })

      expect(res.status).toBeCalledWith(200)
      done()
    })
  })
})
