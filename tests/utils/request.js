module.exports = {
  request: (args = {}) => { 
    const {
      body = {},
      headers = {},
      query = {},
      params = {}
    } = args
    return {
      body,
      headers,
      params,
      query
    }
  },

  response: () => {
    res = {}
    res.json = jest.fn().mockReturnValue(res)
    res.status = jest.fn().mockReturnValue(res)
    return res
  }
}
